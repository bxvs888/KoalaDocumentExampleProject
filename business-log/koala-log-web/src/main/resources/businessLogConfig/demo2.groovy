package org.openkoala

import org.dayatang.domain.InstanceFactory
import org.openkoala.example.application.PersonInfoApplication
import org.openkoala.example.domain.PersonInfo

class PersonInfoApplicationImpl {

	def context
	
	def savePersonInfo() {
		"${getPreTemplate()}:创建个人信息,名字为:${context._param0.name}"
	}

    def pageQueryPersonInfo() {
    	[category:"查询类", log:"查询个人信息列表"]
    }
    
    def removePersonInfo() {
    	//PersonInfoApplication personInfoApplication = InstanceFactory.getInstance(PersonInfoApplication.class)
    	//String name = personInfoApplication.getPersonInfo(577).getName()
    	//"删除用户信息：名称为：" + name
		"删除用户信息"
    }
    
    def getPreTemplate(){
        "${context._user}-"
    }
    
}