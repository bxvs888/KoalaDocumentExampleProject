package com.xiaokaceng.demo.core;

import javax.persistence.Entity;

import org.openkoala.koala.commons.domain.KoalaAbstractEntity;

@Entity
public class Book extends KoalaAbstractEntity {

	private static final long serialVersionUID = -6610967887197177419L;

	private String name;

	private String title;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String[] businessKeys() {
		// TODO Auto-generated method stub
		return null;
	}

}
