
package com.xiaokaceng.demo.application.core;

import java.util.List;
import org.dayatang.querychannel.Page;
import com.xiaokaceng.demo.application.dto.*;

public interface BookApplication {

	public BookDTO getBook(Long id);
	
	public BookDTO saveBook(BookDTO book);
	
	public void updateBook(BookDTO book);
	
	public void removeBook(Long id);
	
	public void removeBooks(Long[] ids);
	
	public List<BookDTO> findAllBook();
	
	public Page<BookDTO> pageQueryBook(BookDTO book, int currentPage, int pageSize);
	

}

