package org.openkoala.security.core;

public class UnmodifiableSetException extends SecurityException{

	private static final long serialVersionUID = -8271861359729175728L;

	public UnmodifiableSetException() {
		super();
	}

	public UnmodifiableSetException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnmodifiableSetException(String message) {
		super(message);
	}

	public UnmodifiableSetException(Throwable cause) {
		super(cause);
	}
	
	
}
