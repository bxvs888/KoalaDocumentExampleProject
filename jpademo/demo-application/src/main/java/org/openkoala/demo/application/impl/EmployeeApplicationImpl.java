package org.openkoala.demo.application.impl;

import java.util.List;
import java.util.ArrayList;
import java.text.MessageFormat;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import org.dayatang.domain.InstanceFactory;
import org.openkoala.demo.application.EmployeeApplication;
import org.openkoala.demo.core.domain.Employee;
import org.openkoala.demo.core.domain.Organization;

@Named
public class EmployeeApplicationImpl implements EmployeeApplication {

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Employee getEmployee(Long id) {
		Employee employee = Employee.load(Employee.class, id);
		
		return employee;
	}
	
	public Employee saveEmployee(Employee employee) {
		employee.save();
		return employee;
	}
	
	public void updateEmployee(Employee employee) {
		employee .save();
	}
	
	public void removeEmployee(Long  id) {
		this.removeEmployees(new Long[] { id });
	}
	
	public void removeEmployees(Long[] ids) {
		for (int i = 0; i < ids.length; i++) {
			Employee employee = Employee.load(Employee.class, ids[i]);
			employee.remove();
		}
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Employee> findAllEmployee() {
		return Employee.findAll(Employee.class);
	}

	public void assignEmployeeToOrganization(Employee employee,
			Organization organization) {
		employee.assignTo(organization);
	}

	public List<Employee> findEmployeesByAgeRange(Integer from, Integer to) {
		return Employee.findByAgeRange(from, to);
	}
	
}
